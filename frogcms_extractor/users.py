import sys
import os
import sqlite3
import unidecode


def get_connection(file_path):
    if not os.path.exists(file_path):
        print('File does not exist {}'.format(file_path), file=sys.stderr)
        sys.exit(1)
    c = sqlite3.connect(':memory:')
    with open(file_path, 'r') as f:
        c.executescript(f.read())
    return c


def parse_user(user):

    return {
        'id': user[0],
        'name': user[1],
        'surname': user[2],
        'mail': user[3],
        'password': user[4],
        'number': user[5],
        'address': user[6],
        'dob': user[7],
        'photo1': user[8],
        'photo2': user[9],
        'photo3': user[10],
        'level': user[11],
        'department': user[12],
        'kv_asd_date': user[13],
        'kv_asd_place': user[14],
        'philistine_date': user[15],
        'other_scout_org': user[16],
        'vow_dates_places': user[17],
        'studies': user[18],
        'jobs': user[19],
        'other_org': user[20],
        'skype': user[21],
        'hobby': user[22],
        'user_created': user[23],
        'create_date': user[24],
        'create_time': user[25],
        'user_updated': user[26],
        'update_date': user[27],
        'update_time': user[28],
        'enabled': user[29],
        'allow_gallery': user[30]
        }


logins = []

def get_login_name(user):
    login = '{}.{}'.format(unidecode.unidecode(user['name']).strip(), unidecode.unidecode(user['surname']).strip()).lower()
    i = 1
    while login in logins:
        login = '{}{}'.format(login, i)
        i += 1
    
    logins.append(login)

    return login

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print('Supply file path to db file', file=sys.stderr)
        sys.exit(1)

    output_file = 'users.sh'
    if len(sys.argv) == 3:
        output_file = sys.argv[2]

    conn = get_connection(sys.argv[1])
    c = conn.cursor()
    c.execute('SELECT * FROM module_members')
    with open(output_file, 'w') as f:
        for user in c.fetchall():
            u = parse_user(user)
            login_name = get_login_name(u)
            print(login_name)
            f.write('wp user create {login} {email} \
                     --role=author \
                     --user_registered={registered} \
                     --display_name={name} \
                     --first_name={name} \
                     --last_name={surname};\n'.format(login=login_name, email=u['mail'], registered=u['create_date'], name=u['name'], surname=u['surname']))
            for m in ['kv_asd_date', 'kv_asd_place', 'number', 'other_org', 'other_scout_org', 'philistine_date', 'photo1', 'skype',
                      'studies', 'vow_dates_places']:
                if u[m]:
                    f.write('wp user meta add {login} {key} "{value}";\n'.format(login=login_name, key=m, value=u[m]))
